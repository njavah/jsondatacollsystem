package panel;

import dto.JsonData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.ShowDateService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;

public class CreatJsonPanel {
    private Logger logger = LoggerFactory.getLogger(CreatJsonPanel.class);
    private JLabel fileNameLabel = new JLabel("文件名");
    private JTextField fileName = new JTextField(35);
    private JButton createButton;
    private Container container;
    private int width = 1200;//界面的宽度
    private int height = 800;//界面的高度

    private FlowLayout layout = new FlowLayout(FlowLayout.CENTER);//布局格式
    private JFrame jFrame = new JFrame("生成数据");
    private Font font =  new Font("楷体",Font.ITALIC,40);//字体格式
    private Font pathFont = new Font("TimesRoman",Font.PLAIN,25);
    private Dimension dimension = new Dimension(width,height);
    private ShowDateService showDateService;
    public CreatJsonPanel(){
        showDateService = new ShowDateService();
        container = jFrame.getContentPane();
        layout.setVgap(width / 6);
        container.setLayout(layout);
        createButton = new JButton("生成文本");
        createButton.setFont(pathFont);
        createButton.setForeground(Color.white);
        createButton.setBackground(new Color(0,145,0));
        createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    JsonData jsonData = new JsonData();
                    jsonData.setJsonFileName(fileName.getText());
                    showDateService.createJson(jsonData);
                    JOptionPane.showMessageDialog(null, "生成成功", "【成功】", JOptionPane.INFORMATION_MESSAGE);
                }catch (FileNotFoundException fileNotFoundException ){
                    JOptionPane.showMessageDialog(null, "该文件不存在", "【出错啦】", JOptionPane.ERROR_MESSAGE);
                    logger.error("UploadPanel-ThreadName:{},Exception=>{},msg=>{}"+Thread.currentThread().getName(),fileNotFoundException.getMessage(),fileNotFoundException);
                }catch (Exception exception){
                    //生成文件失败
                    JOptionPane.showMessageDialog(null, "生成失败", "【出错啦】", JOptionPane.ERROR_MESSAGE);
                    logger.error("UploadPanel-ThreadName:{},Exception=>{},msg=>{}"+Thread.currentThread().getName(),exception.getMessage(),exception);
                }
            }
        });
        fileNameLabel.setFont(font);
        fileName.setFont(pathFont);
        container.add(fileNameLabel);
        container.add(fileName);
        container.add(createButton);
        ((JPanel) container).setOpaque(false); //注意这里，将内容面板设为透明。这样LayeredPane面板中的背景才能显示出来。
        jFrame.setSize(dimension);
        jFrame.setVisible(true);
        jFrame.setResizable(false);
    }
}
