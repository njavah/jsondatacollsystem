package panel;

import dto.AnalysisData;
import dto.JsonData;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import service.UploadService;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.Observable;
import java.util.Observer;

/**
 * @Author:NiHai
 * @Description 上传数据界面
 * @Date: 12:37 2019/1/21
 */
public class UploadPanel{
    private Logger logger = LoggerFactory.getLogger(UploadPanel.class);
    private JFrame jFrame = new JFrame("上传数据");
    private Container container;//面板
    private FlowLayout layout = new FlowLayout(FlowLayout.CENTER);//布局格式
    private JButton scanButton;//浏览选择目录
    private JButton uploadButton;//上传按钮
    private JProgressBar taskProgress = new JProgressBar(0,100);//任务进度
    private JLabel pathLabel = new JLabel("文件路径:");//文件路径标签
    private JTextField path;//文件路径
    private int width = 1200;//界面的宽度
    private int height = 800;//界面的高度
    private int buttonWidth = 200;//按鈕長度
    private int buttonHeight = 100;//按鈕高度
    private Font font =  new Font("楷体",Font.ITALIC,40);//字体格式
    private Font pathFont = new Font("TimesRoman",Font.PLAIN,25);
    private Dimension dimension = new Dimension(width,height);
    private Dimension buttonDimension = new Dimension(buttonWidth,buttonHeight);
    private  AnalysisData analysisData ;
    private UploadService uploadService;
    public UploadPanel() {
        analysisData = new AnalysisData(this);
        uploadService = new UploadService(this);
        container = jFrame.getContentPane();
        container.setBackground(Color.pink);
        ImageIcon background = new ImageIcon("C:\\Users\\倪海\\Desktop\\upload.jpg");// 背景图片
        JLabel label = new JLabel(background);// 把背景图片显示在一个标签里面
        // 把标签的大小位置设置为图片刚好填充整个面板
        label.setBounds(0, 0, background.getIconWidth(), background.getIconHeight());
        ((JPanel) jFrame.getContentPane()).setOpaque(false);
        jFrame.getLayeredPane().setLayout(null);
        // 把背景图片添加到分层窗格的最底层作为背景
        jFrame.getLayeredPane().add(label, new Integer(Integer.MIN_VALUE));
        layout.setVgap(width / 6);
        //layout.setHgap(height / 2);
        container.setLayout(layout);
        scanButton = new JButton("浏览");
        scanButton.setFont(pathFont);
        scanButton.setForeground(Color.white);
        scanButton.setBackground(new Color(0,145,0));
        scanButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
                int ret = chooser.showOpenDialog(jFrame);
                if (ret == JFileChooser.APPROVE_OPTION) {
                    File file = chooser.getSelectedFile();
                    path.setText(file.getPath());
                }
            }
        });
        uploadButton = new JButton("上传");
        uploadButton.setFont(pathFont);
        uploadButton.setForeground(Color.white);
        uploadButton.setBackground(Color.blue);
        uploadButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                taskProgress.setVisible(true);
                taskProgress.setFont(pathFont);
                taskProgress.setStringPainted(true);
                taskProgress.setPreferredSize(new Dimension(400,20));
                taskProgress.setBackground(Color.WHITE);
                taskProgress.setForeground(Color.GREEN);
                //当点击上传时 后台开始解析文件并存储到数据库，简单的先模拟

                Thread thread = new Thread(new Runnable() {
                    public void run() {
                        JsonData jsonData = null;
                        try {
                            jsonData = analysisData.returnDataByPath(path.getText());
                            uploadService.insertData(jsonData);
                        }catch (Exception e){

                            logger.error("UploadPanel-ThreadName:{},Exception=>{},msg=>{}"+Thread.currentThread().getName(),e.getMessage(),e);
                            //e.printStackTrace();
                        }
                    }
                });
                thread.start();
            }
        });
        path = new JTextField(35);
        pathLabel.setFont(font);
        path.setFont(pathFont);
        container.add(pathLabel);
        container.add(path);
        container.add(scanButton);
        container.add(uploadButton);
        container.add(taskProgress);
        taskProgress.setVisible(false);
        ((JPanel) container).setOpaque(false); //注意这里，将内容面板设为透明。这样LayeredPane面板中的背景才能显示出来。
        jFrame.setSize(dimension);
        jFrame.setVisible(true);
        jFrame.setResizable(false);
    }

    public static void main(String[] args) {
        UploadPanel uploadPanel = new UploadPanel();
    }

    public void update() {
        if(AnalysisData.status.getName().contains("失败")){
            taskProgress.setString(AnalysisData.status.getName());
            taskProgress.setValue(100);
            taskProgress.setForeground(Color.red);
        }else{
            taskProgress.setString(AnalysisData.status.getName());
            taskProgress.setForeground(Color.BLUE);
            taskProgress.setValue(taskProgress.getValue()+15);
            if((taskProgress.getValue()+15)>=90)
                taskProgress.setValue(100);
        }
    }
}

