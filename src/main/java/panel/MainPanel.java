package panel;

import org.springframework.stereotype.Service;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @Author:NiHai
 * @Description  主界面
 * @Date: 10:47
 */
@Service
public class MainPanel {
    private JFrame jFrame = new JFrame("JSON数据采集系统");
    private Container container;//面板
    private FlowLayout layout = new FlowLayout(FlowLayout.CENTER);//布局格式
    private JButton uploadButton;//上传数据按钮
    private JButton createButton;//生成JSON文件按钮
    private JButton showButton;//显示数据按钮
    private int width = 1200;//界面的宽度
    private int height = 800;//界面的高度
    private int buttonWidth = 200;//按鈕長度
    private int buttonHeight = 100;//按鈕高度
    private Font font =  new Font("TimesRoman",Font.BOLD,40);//字体格式
    private Dimension dimension = new Dimension(width,height);
    private Dimension buttonDimension = new Dimension(buttonWidth,buttonHeight);
    public MainPanel(){
        container = jFrame.getContentPane();
        container.setBackground(Color.pink);
        ImageIcon background = new ImageIcon("C:\\Users\\倪海\\Desktop\\timg.jpg");// 背景图片
        JLabel label = new JLabel(background);// 把背景图片显示在一个标签里面
        // 把标签的大小位置设置为图片刚好填充整个面板
        label.setBounds(0, 0, background.getIconWidth(),background.getIconHeight());
        ((JPanel) jFrame.getContentPane()).setOpaque(false);
        jFrame.getLayeredPane().setLayout(null);
        // 把背景图片添加到分层窗格的最底层作为背景
        jFrame.getLayeredPane().add(label, new Integer(Integer.MIN_VALUE));
        layout.setVgap(width/6);
        layout.setHgap(height/2);
        container.setLayout(layout);
        uploadButton = new JButton("上传数据");
        uploadButton.setFont(font);
        uploadButton.setSize(buttonDimension);
        uploadButton.setForeground(Color.white);
        uploadButton.setBackground(new Color(0,175,255));
        createButton = new JButton("生成JSON文本");
        createButton.setFont(font);
        createButton.setForeground(Color.white);
        createButton.setBackground(new Color(0,175,255));
        createButton.setSize(buttonDimension);
        showButton = new JButton("查询数据");
        showButton.setFont(font);
        showButton.setForeground(Color.white);
        showButton.setBackground(new Color(0,175,255));
        showButton.setSize(buttonDimension);
        uploadButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                UploadPanel uploadPanel = new UploadPanel();
            }
        });
        createButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CreatJsonPanel creatJsonPanel = new CreatJsonPanel();
            }
        });
        showButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ShowDataPanel showDataPanel = new ShowDataPanel();
            }
        });
        container.add(uploadButton);
        container.add(createButton);
        container.add(showButton);
        ((JPanel)container).setOpaque(false); //注意这里，将内容面板设为透明。这样LayeredPane面板中的背景才能显示出来。
        jFrame.setSize(dimension);
        jFrame.setVisible(true);
        jFrame.setResizable(false);
    }

    public static void main(String[] args) {
        new MainPanel();
    }

}
