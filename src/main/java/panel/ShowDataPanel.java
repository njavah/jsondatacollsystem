package panel;

import com.alibaba.fastjson.JSON;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dto.ControlParameter;
import dto.JsonData;
import dto.OperationProcess;
import org.apache.commons.beanutils.BeanUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import service.ShowDateService;
import utils.TimeUtils;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.FileNotFoundException;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.util.*;
import java.util.List;

public class ShowDataPanel {
    private Logger logger = LoggerFactory.getLogger(CreatJsonPanel.class);
    private JLabel fileNameLabel = new JLabel("文件名");
    private JTextField fileName = new JTextField(10);
    private JLabel batchNumberLabel = new JLabel("生产编号");
    private JTextField batchNumber = new JTextField(10);
    private JLabel startDateLabel = new JLabel("开始日期");
    private JTextField startDate= new JTextField(10);
    private JLabel endDateLabel = new JLabel("结束日期");
    private JTextField endDate= new JTextField(10);
    private  JScrollPane scrollPane  =  null;
    private JButton showButton;
    private Container container;
    private int width = 1800;//界面的宽度
    private int height = 1000;//界面的高度

    private JTable talbe=new JTable();
    private JScrollPane jScrollPane = new JScrollPane(talbe);




    private FlowLayout layout = new FlowLayout(FlowLayout.CENTER);//布局格式
    private JFrame jFrame = new JFrame("查询数据");
    private Font font =  new Font("楷体",Font.ITALIC,40);//字体格式
    private Font pathFont = new Font("TimesRoman",Font.PLAIN,25);
    private Dimension dimension = new Dimension(width,height);
    private ShowDateService showDateService;

    public ShowDataPanel(){
        showDateService = new ShowDateService();
        container = jFrame.getContentPane();
        layout.setVgap(width / 6);
        container.setLayout(layout);
        showButton = new JButton("查询");
        showButton.setFont(pathFont);
        showButton.setForeground(Color.white);
        showButton.setBackground(new Color(0,145,0));
        showButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    String startTimeStr = startDate.getText();
                    String endTimeStr = endDate.getText();
                    Long startTime = null;
                    Long endTime = null;
                    if(!StringUtils.isEmpty(startTimeStr)){
                        startTime = TimeUtils.stringShortToMillisecond(startTimeStr);
                    }
                    if(!StringUtils.isEmpty(endTimeStr)){
                        endTime = TimeUtils.stringShortToMillisecond(endTimeStr);
                    }
                    Map<String,Object> map = new HashMap<String,Object>();
                    map.put("fileName",fileName.getText());
                    map.put("batchNumber",batchNumber.getText());
                    map.put("startTime",startTime);
                    map.put("endTime",endTime);
                    final List<JsonData> list = showDateService.listJsonData(map);
                    System.out.println(JSON.toJSON(list).toString());
                    System.out.println(list.size());
                    //对list数据进行显示
                    JsonData jsonData = new JsonData();
                    Class c1 = jsonData.getClass();
                    Field[] fields = c1.getDeclaredFields();
                    Vector<String> arrayList = new Vector<String>();
                   /* for(Field field:fields){
                        arrayList.add(field.getName());
                    }*/
                    arrayList.add("主键");
                    arrayList.add("文件名");
                    arrayList.add("生产名字");
                    arrayList.add("生产日期");
                    arrayList.add("协调设备");
                    arrayList.add("批量编号");
                    arrayList.add("生产数量");
                    arrayList.add("加工数量");
                    arrayList.add("生产温度");
                    arrayList.add("抽样时间");
                    arrayList.add("抽样结论");
                    arrayList.add("包装形式");
                    arrayList.add("数量填充");
                    arrayList.add("最终重量");
                    arrayList.add("填充路线编号");
                    arrayList.add("主要练习");
                    arrayList.add("审核信息");
                    arrayList.add("上报时间");

                    Vector data = new Vector();

                    for(int  i=0;i<list.size();i++){
                        Vector rowData = new Vector();
                      Class cls = list.get(i).getClass();
                      for(Field field:cls.getDeclaredFields()){
                          field.setAccessible(true);
                          rowData.add(field.get(list.get(i)));
                      }
                      data.add(rowData);
                    }
                    talbe.setModel(new DefaultTableModel(data,arrayList));
                    talbe.setRowHeight(50);
                    TableColumnModel tableColumnModel = talbe.getColumnModel();
                    for(int i=0;i<tableColumnModel.getColumnCount();i++){
                        if(i==3 || i==5 || i==tableColumnModel.getColumnCount()-1){
                            tableColumnModel.getColumn(i).setPreferredWidth(170);
                        }else{
                            tableColumnModel.getColumn(i).setPreferredWidth(70);
                        }

                    }
                    for(int i = 0;i < list.size() ; i++){
                        setOneRowBackgroundColor(talbe,i,Color.blue);
                    }
                    talbe.setFont(new Font("Menu.font", Font.PLAIN, 14));
                    talbe.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
                    talbe.addMouseListener(new MouseAdapter() {
                        @Override
                        public void mouseClicked(MouseEvent e) {
                            int row = talbe.rowAtPoint(e.getPoint());
                            int col = talbe.columnAtPoint(e.getPoint());
                            System.out.println("被点击了:"+row+","+col);
                            String message = "";
                            Boolean isShow = false;
                            if(col == 1){
                                ControlParameter controlParameter = list.get(row).getControlParameter();
                                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                message = gson.toJson(controlParameter);
                                isShow = true;
                            }else if(col==2){
                                ArrayList<OperationProcess> operationProcessArrayList = list.get(row).getProcessArrayList();
                                ControlParameter controlParameter = list.get(row).getControlParameter();
                                Gson gson = new GsonBuilder().setPrettyPrinting().create();
                                message = gson.toJson(operationProcessArrayList);
                                isShow = true;
                            }
                            if(isShow)
                            new MyJDialog(jFrame,message).setVisible(true);
                        }
                    });

                }catch (FileNotFoundException fileNotFoundException ){
                    JOptionPane.showMessageDialog(null, "该文件不存在", "【出错啦】", JOptionPane.ERROR_MESSAGE);
                    logger.error("UploadPanel-ThreadName:{},Exception=>{},msg=>{}"+Thread.currentThread().getName(),fileNotFoundException.getMessage(),fileNotFoundException);
                }catch (ParseException parseException){
                    //时间解析失败
                    JOptionPane.showMessageDialog(null, "日期格式有误", "【出错啦】", JOptionPane.ERROR_MESSAGE);
                    logger.error("UploadPanel-ThreadName:{},Exception=>{},msg=>{}"+Thread.currentThread().getName(),parseException.getMessage(),parseException);
                }catch (Exception exception){
                    //生成文件失败
                    JOptionPane.showMessageDialog(null, "查询失败", "【出错啦】", JOptionPane.ERROR_MESSAGE);
                    logger.error("UploadPanel-ThreadName:{},Exception=>{},msg=>{}"+Thread.currentThread().getName(),exception.getMessage(),exception);
                }
            }
        });
        fileNameLabel.setFont(font);
        fileName.setFont(pathFont);
        batchNumberLabel.setFont(font);
        batchNumber.setFont(pathFont);
        startDateLabel.setFont(font);
        startDate.setFont(pathFont);
        endDateLabel.setFont(font);
        endDate.setFont(pathFont);
        container.add(fileNameLabel);
        container.add(fileName);
        container.add(batchNumberLabel);
        container.add(batchNumber);
        container.add(startDateLabel);
        container.add(startDate);
        container.add(endDateLabel);
        container.add(endDate);
        container.add(showButton);
        jScrollPane.setSize(dimension);
        container.add(jScrollPane);
        ((JPanel) container).setOpaque(false); //注意这里，将内容面板设为透明。这样LayeredPane面板中的背景才能显示出来。
        jFrame.setSize(dimension);
        jFrame.setVisible(true);
        //jFrame.setResizable(false);
    }

    public static void setOneRowBackgroundColor(JTable table, final  int rowIndex,
                                                final Color color) {
        try {
            DefaultTableCellRenderer tcr = new DefaultTableCellRenderer() {

                public Component getTableCellRendererComponent(JTable table,
                                                               Object value, boolean isSelected, boolean hasFocus,
                                                               int row, int column) {
                    Color color1 = new Color(153,204,204);
                    setBackground(color1);
                    setForeground(Color.black);

                    return super.getTableCellRendererComponent(table, value,
                            isSelected, hasFocus, row, column);
                }
            };
            int columnCount = table.getColumnCount();
            for (int i = 0; i < columnCount; i++) {
                table.getColumn(table.getColumnName(i)).setCellRenderer(tcr);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}

class MyJDialog extends JDialog{
    //本实例代码可以看到，JDialog窗体和JFrame窗体形式基本相同，甚至在设置窗体的特性
    //时调用的方法名称都基本相同，如设置窗体的大小，设置窗体的关闭状态等
    public MyJDialog(JFrame frame,String message){//定义一个构造方法
        //实例化一个JDialog类对象，指定对话框的父窗体，窗体标题，和类型
        super(frame,"第一个JDialog窗体",true);
        Container container=getContentPane();//创建一个容器
        container.add(new JLabel("这是一个对话框"));//在容器中添加标签
        container.add(new JTextArea(message));
        container.setBackground(Color.green);
        setBounds(120,120,100,100);
    }
}
