package dao;

import dto.OperationProcess;

import java.util.List;

public interface ProcessMapper {
    int insertProcess(List<OperationProcess> processList);
}
