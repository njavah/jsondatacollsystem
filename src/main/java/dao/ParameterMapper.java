package dao;

import dto.ControlParameter;

import java.util.List;

public interface ParameterMapper {
    int insertParameter(List<ControlParameter> parameterList);
}
