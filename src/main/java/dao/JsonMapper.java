package dao;


import dto.JsonData;

import java.util.List;
import java.util.Map;

public interface JsonMapper {
    int insertJsonData(JsonData jsonData);
    JsonData returnJsonDataByFileName(JsonData jsonData);
    List<JsonData> listJsonData(Map<String,Object> map);
}
