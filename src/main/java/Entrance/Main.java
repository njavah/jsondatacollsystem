package Entrance;

import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import panel.MainPanel;
import panel.UploadPanel;

/**
 * @Author:NiHai
 * @Description 程序执行入口
 * @Date: 10:50 2019/1/21
 */
@Service
public class Main {
    public static  ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("classpath:applicationcontext.xml");
    public Main(){
        MainPanel mainPanel = new MainPanel();
    }
    public static void main(String[] args) {
        context.start();
        //C:\Users\倪海\Desktop\SN00016_20180822.json
    }
}
