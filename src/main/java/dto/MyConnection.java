package dto;

import java.sql.*;

public class MyConnection {
    public static Connection getConnection(){
        Connection con = null;
        //驱动程序名
        String driver = "com.mysql.jdbc.Driver";
        //URL指向要访问的数据库名mydata
        String url = "jdbc:mysql://localhost:3306/nhdata";
        //MySQL配置时的用户名
        String user = "root";
        //MySQL配置时的密码
        String password = "nh500239188014nh";
        try {
            //加载驱动程序
            Class.forName(driver);
            //1.getConnection()方法，连接MySQL数据库！！
            con = DriverManager.getConnection(url,user,password);
            //2.创建statement类对象，用来执行SQL语句！
        } catch(ClassNotFoundException e) {
            //数据库驱动类异常处理
            e.printStackTrace();
        } catch(SQLException e) {
            //数据库连接失败异常处理
            e.printStackTrace();
        }catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
        }finally{
            return con;
        }
    }

    public static void main(String[] args) {
        Connection connection = MyConnection.getConnection();
        try {
            Statement statement = connection.createStatement();
        }catch (Exception e){
            e.printStackTrace();
        }

    }
}
