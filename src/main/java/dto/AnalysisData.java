package dto;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import panel.UploadPanel;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Observable;
import java.util.TimeZone;

/**
 * @Author:NiHai
 * @Description 解析数据工具类
 * @Date: 18:21 2019/1/23
 */
public class AnalysisData extends Observable {
    private Logger logger = LoggerFactory.getLogger(AnalysisData.class);
    public static     AnalysisStatus status = AnalysisStatus.READ;
    private UploadPanel uploadPanel = null;
    /**
     *  通过文件路径解析文件
     * @param path 文件路径
     * @return 返回数据对应的实体类
     */
    public AnalysisData(){

    }

    public AnalysisData(UploadPanel uploadPanel){
        this.uploadPanel = uploadPanel;
    }

    public String returnResult(String path){
        logger.info("returnData     ByPath:path=>{}",path);
        StringBuilder realData = new StringBuilder();
        String tempText;
        InputStreamReader isr;
        BufferedReader br = null;
        File file = null;
        try {
            file = new File(path);
            if(file.isFile() && file.exists()){
                status = AnalysisStatus.READ;
                uploadPanel.update();
                isr = new InputStreamReader(new FileInputStream(file), "utf-8");
                br = new BufferedReader(isr);
                while ((tempText= br.readLine()) != null) {
                    realData.append(tempText);
                }
            }else{
                logger.error("returnDataByPath:文件路径 {}错误！",file.getPath());
                status = AnalysisStatus.READFAIL;
                uploadPanel.update();
                //前面会检测
            }
        }catch (Exception e){
            logger.error("returnDataByPath:file=>{},Exception=>{},msg=>{}",file,e.getMessage(),e);
            status = AnalysisStatus.READFAIL;
            uploadPanel.update();
            throw new RollbackException(e.toString());
            //读取文件失败
        }finally {
            if(br!=null){
                try{
                    br.close();
                }catch (Exception e){
                    e.printStackTrace();
                }

            }
        }
        status = AnalysisStatus.READSUCCESS;
        uploadPanel.update();
        return realData.toString();
    }

    public  JsonData returnDataByPath(String path){
        return  returnData(returnResult(path));
    }

    /**
     *  通过文件数据解析
     * @param data 真实的数据
     * @return 返回数据对应的实体类
     */
    public  JsonData returnData(String data){
        JsonData jsonData = new JsonData();
        status = AnalysisStatus.ANALY;
        uploadPanel.update();
        try {
            JSONObject jsonObject =  JSONObject.fromObject(data);
            ControlParameter controlParameter = new ControlParameter();
            JSONArray controlParameterArray = (JSONArray)jsonObject.get("ControlParameters");
            jsonData.setProductDate(getCalendar((String)jsonObject.get("ProductDate")).getTimeInMillis());
            JSONObject parameterObject = ((JSONObject)controlParameterArray.get(0));
            controlParameter.setParmShowName((String)parameterObject.get("ParmShowName"));
            controlParameter.setParmVal(Float.valueOf((String)parameterObject.get("ParmVal")));
            controlParameter.setParmUD((String)parameterObject.get("ParmUD"));
            controlParameter.setForignId(jsonData.getProductDate());
            jsonData.setControlParameter(controlParameter);
            jsonData.setJsonFileName((String)jsonObject.get("JsonFileName"));
            jsonData.setProductName((String)jsonObject.get("ProductName"));
            jsonData.setHarmonizationEquipment((String)jsonObject.get("HarmonizationEquipment"));
            jsonData.setBatchNumber((String)jsonObject.get("BatchNumber"));
            jsonData.setProductionQuantity((String)jsonObject.get("ProductionQuantity"));
            jsonData.setProcessNumber((String)jsonObject.get("ProcessNumber"));
            jsonData.setProductionTemperature((String)jsonObject.get("ProductionTemperature"));
            jsonData.setSamplingTime((String)jsonObject.get("SamplingTime"));
            jsonData.setSamplingConclusion((String)jsonObject.get("SamplingConclusion"));
            jsonData.setPackingForm((String)jsonObject.get("PackingForm"));
            jsonData.setQuantityFilling((String)jsonObject.get("PackingForm"));
            jsonData.setFinalWeight((String)jsonObject.get("FinalWeight"));
            jsonData.setFillingLineNumber((String)jsonObject.get("FillingLineNumber"));
            jsonData.setMainExercise((String)jsonObject.get("MainExercise"));
            jsonData.setToReview((String)jsonObject.get("ToReview"));
            jsonData.setReportTime(getCalendar((String)jsonObject.get("ReportTime")).getTimeInMillis());
            JSONArray jsonArray = (JSONArray)jsonObject.get("OperationProcessList");
            ArrayList<OperationProcess> processArrayList = new ArrayList<OperationProcess>();
            for(int i=0;i<jsonArray.size();i++){
                OperationProcess operationProcess = new OperationProcess();
                operationProcess.setForignId(jsonData.getProductDate());
                operationProcess.setFKShowName((String)((JSONObject)jsonArray.get(i)).get("FKShowName"));
                operationProcess.setWLShowName((String)((JSONObject)jsonArray.get(i)).get("WLShowName"));
                operationProcess.setSetShowVal((String)((JSONObject)jsonArray.get(i)).get("SetShowVal"));
                operationProcess.setBeginTime(getCalendar((String)((JSONObject)jsonArray.get(i)).get("BeginTime")).getTimeInMillis()); //需要转换参数
                operationProcess.setEndTime(getCalendar((String)((JSONObject)jsonArray.get(i)).get("EndTime")).getTimeInMillis());
                JSONArray fkcArray = (JSONArray)((JSONObject)jsonArray.get(i)).get("FKControlParameters");
                ArrayList<ControlParameter> cpList = new ArrayList<ControlParameter>();
                ControlParameter fkcCP ;
                for(int j=0;j<fkcArray.size();j++){
                    fkcCP = new ControlParameter();
                    fkcCP.setParmShowName((String)((JSONObject)fkcArray.get(j)).get("ParmShowName"));
                    fkcCP.setParmVal(Float.valueOf((String)((JSONObject)fkcArray.get(j)).get("ParmVal")));
                    fkcCP.setParmUD((String)((JSONObject)fkcArray.get(j)).get("ParmUD"));
                    fkcCP.setForignId(jsonData.getProductDate());
                    cpList.add(fkcCP);
                }
                operationProcess.setParameterArrayList(cpList);
                operationProcess.setEndState((String)((JSONObject)jsonArray.get(i)).get("EndState"));
                processArrayList.add(operationProcess);
            }
            jsonData.setProcessArrayList(processArrayList);
            System.out.println(jsonData.toString());
        }catch (Exception e){
            logger.error("returnData:data=>{},Exception=>{},msg=>{}",data,e.getMessage(),e);
            status = AnalysisStatus.ANALYFAIL;
            uploadPanel.update();
            jsonData = null;
            throw new RollbackException(e.toString());
        }
        status = AnalysisStatus.ANALYSUCCESS;
        uploadPanel.update();
        return jsonData;
    }



    public static void main(String[] args) {
       // AnalysisData analysisData = new AnalysisData();
       // analysisData.returnDataByPath("C:\\Users\\倪海\\Desktop\\SN00016_20180822.json");
        Calendar calendar = new AnalysisData().getCalendar("2018-08-22");
        //long x = calendar.getTimeInMillis();
        System.out.println(calendar.getTimeInMillis());
        System.out.println(calendar.getTime().toString());
        Calendar calendar1 = Calendar.getInstance();

    }

    public Calendar getCalendar(String date){
        Calendar calendar = Calendar.getInstance();
        String[] strs = date.split(" ");
        if(strs.length==1){
            String[] dates = strs[0].split("-");
            calendar.set(Integer.valueOf(dates[0]),Integer.valueOf(dates[1])-1,Integer.valueOf(dates[2]),0,0,0);
        }else if(strs.length==2){
            String[] dates = strs[0].split("-");
            String[] mins = strs[1].split(":");
            calendar.set(Integer.valueOf(dates[0]),Integer.valueOf(dates[1])-1,Integer.valueOf(dates[2]),Integer.valueOf(mins[0]),Integer.valueOf(mins[1]),Integer.valueOf(mins[2]));
        }else{
            //传入数据格式有问题
        }
        return calendar;
    }
}
