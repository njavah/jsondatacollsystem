package dto;

public enum AnalysisStatus {
    READ("读取文件中",1),//读取文件
    READSUCCESS("读取文件成功",2),//读取成功
    READFAIL("读取文件失败",3),//读取失败
    ANALY("解析文件中",4),//解析文件
    ANALYSUCCESS("解析文件成功",5),//解析成功
    ANALYFAIL("解析文件失败",6),//解析失败
    STORE("存储数据中",7),
    STOREFAIL("存储数据失败",8),
    STORESUCCESS("存储数据成功",9);
    private String name;
    private int status;
    private AnalysisStatus(String name,int status){
        this.name = name;
        this.status = status;
    }
    public String getName(){
        return name;
    }
}
