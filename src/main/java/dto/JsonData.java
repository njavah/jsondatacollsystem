package dto;


import java.util.ArrayList;

/**
 * @Author:NiHai
 * @Description JSON数据对应的实体类
 * @Date: 18:18 2019/1/23
 */
public class JsonData {
    private int id;
    private ControlParameter controlParameter;
    private ArrayList<OperationProcess> processArrayList;
    private String JsonFileName;
    private String ProductName;
    private long ProductDate;
    private String HarmonizationEquipment;
    private String BatchNumber;
    private String ProductionQuantity;
    private String ProcessNumber;
    private String ProductionTemperature;
    private String SamplingTime;
    private String SamplingConclusion;
    private String PackingForm;
    private String QuantityFilling;
    private String FinalWeight;
    private String FillingLineNumber;
    private String MainExercise;
    private String ToReview;
    private long ReportTime;

    public ControlParameter getControlParameter() {
        return controlParameter;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setControlParameter(ControlParameter controlParameter) {
        this.controlParameter = controlParameter;
    }

    public ArrayList<OperationProcess> getProcessArrayList() {
        return processArrayList;
    }

    public void setProcessArrayList(ArrayList<OperationProcess> processArrayList) {
        this.processArrayList = processArrayList;
    }

    public String getJsonFileName() {
        return JsonFileName;
    }

    public void setJsonFileName(String jsonFileName) {
        JsonFileName = jsonFileName;
    }

    public String getProductName() {
        return ProductName;
    }

    public void setProductName(String productName) {
        ProductName = productName;
    }

    public long getProductDate() {
        return ProductDate;
    }

    public void setProductDate(long productDate) {
        ProductDate = productDate;
    }

    public String getHarmonizationEquipment() {
        return HarmonizationEquipment;
    }

    public void setHarmonizationEquipment(String harmonizationEquipment) {
        HarmonizationEquipment = harmonizationEquipment;
    }

    public String getBatchNumber() {
        return BatchNumber;
    }

    public void setBatchNumber(String batchNumber) {
        BatchNumber = batchNumber;
    }

    public String getProductionQuantity() {
        return ProductionQuantity;
    }

    public void setProductionQuantity(String productionQuantity) {
        ProductionQuantity = productionQuantity;
    }

    public String getProcessNumber() {
        return ProcessNumber;
    }

    public void setProcessNumber(String processNumber) {
        ProcessNumber = processNumber;
    }

    public String getProductionTemperature() {
        return ProductionTemperature;
    }

    public void setProductionTemperature(String productionTemperature) {
        ProductionTemperature = productionTemperature;
    }

    public String getSamplingTime() {
        return SamplingTime;
    }

    public void setSamplingTime(String samplingTime) {
        SamplingTime = samplingTime;
    }

    public String getSamplingConclusion() {
        return SamplingConclusion;
    }

    public void setSamplingConclusion(String samplingConclusion) {
        SamplingConclusion = samplingConclusion;
    }

    public String getPackingForm() {
        return PackingForm;
    }

    public void setPackingForm(String packingForm) {
        PackingForm = packingForm;
    }

    public String getQuantityFilling() {
        return QuantityFilling;
    }

    public void setQuantityFilling(String quantityFilling) {
        QuantityFilling = quantityFilling;
    }

    public String getFinalWeight() {
        return FinalWeight;
    }

    public void setFinalWeight(String finalWeight) {
        FinalWeight = finalWeight;
    }

    public String getFillingLineNumber() {
        return FillingLineNumber;
    }

    public void setFillingLineNumber(String fillingLineNumber) {
        FillingLineNumber = fillingLineNumber;
    }

    public String getMainExercise() {
        return MainExercise;
    }

    public void setMainExercise(String mainExercise) {
        MainExercise = mainExercise;
    }

    public String getToReview() {
        return ToReview;
    }

    public void setToReview(String toReview) {
        ToReview = toReview;
    }

    public long getReportTime() {
        return ReportTime;
    }

    public void setReportTime(long reportTime) {
        ReportTime = reportTime;
    }

    @Override
    public String toString() {
        return "dto.JsonData{" +
                "controlParameter=" + controlParameter +
                ", processArrayList=" + processArrayList +
                ", JsonFileName='" + JsonFileName + '\'' +
                ", ProductName='" + ProductName + '\'' +
                ", ProductDate=" + ProductDate +
                ", HarmonizationEquipment='" + HarmonizationEquipment + '\'' +
                ", BatchNumber='" + BatchNumber + '\'' +
                ", ProductionQuantity='" + ProductionQuantity + '\'' +
                ", ProcessNumber='" + ProcessNumber + '\'' +
                ", ProductionTemperature='" + ProductionTemperature + '\'' +
                ", SamplingTime='" + SamplingTime + '\'' +
                ", SamplingConclusion='" + SamplingConclusion + '\'' +
                ", PackingForm='" + PackingForm + '\'' +
                ", QuantityFilling='" + QuantityFilling + '\'' +
                ", FinalWeight='" + FinalWeight + '\'' +
                ", FillingLineNumber='" + FillingLineNumber + '\'' +
                ", MainExercise='" + MainExercise + '\'' +
                ", ToReview='" + ToReview + '\'' +
                ", ReportTime=" + ReportTime +
                '}';
    }
}
