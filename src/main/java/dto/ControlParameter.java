package dto;

public class ControlParameter {
    private String ParmShowName;
    private float ParmVal;
    private String ParmUD;
    private long forignId;

    public String getParmShowName() {
        return ParmShowName;
    }

    public void setParmShowName(String parmShowName) {
        ParmShowName = parmShowName;
    }

    public float getParmVal() {
        return ParmVal;
    }

    public void setParmVal(float parmVal) {
        ParmVal = parmVal;
    }

    public String getParmUD() {
        return ParmUD;
    }

    public void setParmUD(String parmUD) {
        ParmUD = parmUD;
    }

    public long getForignId() {
        return forignId;
    }

    public void setForignId(long forignId) {
        this.forignId = forignId;
    }

    @Override
    public String toString() {
        return "dto.ControlParameter{" +
                "ParmShowName='" + ParmShowName + '\'' +
                ", ParmVal=" + ParmVal +
                ", ParmUD='" + ParmUD + '\'' +
                ", forignId=" + forignId +
                '}';
    }
}
