package dto;

import java.util.ArrayList;

public class OperationProcess {
   private int id;
   private String FKShowName;
   private String WLShowName;
   private String SetShowVal;
   private long BeginTime;
   private long EndTime;
   private String EndState;
   private ArrayList<ControlParameter> parameterArrayList;
   private long forignId;

   public int getId() {
      return id;
   }

   public void setId(int id) {
      this.id = id;
   }

   public String getFKShowName() {
      return FKShowName;
   }

   public void setFKShowName(String FKShowName) {
      this.FKShowName = FKShowName;
   }

   public String getWLShowName() {
      return WLShowName;
   }

   public void setWLShowName(String WLShowName) {
      this.WLShowName = WLShowName;
   }

   public String getSetShowVal() {
      return SetShowVal;
   }

   public void setSetShowVal(String setShowVal) {
      SetShowVal = setShowVal;
   }

   public long getBeginTime() {
      return BeginTime;
   }

   public void setBeginTime(long beginTime) {
      BeginTime = beginTime;
   }

   public long getEndTime() {
      return EndTime;
   }

   public void setEndTime(long endTime) {
      EndTime = endTime;
   }

   public String getEndState() {
      return EndState;
   }

   public void setEndState(String endState) {
      EndState = endState;
   }

   public ArrayList<ControlParameter> getParameterArrayList() {
      return parameterArrayList;
   }

   public void setParameterArrayList(ArrayList<ControlParameter> parameterArrayList) {
      this.parameterArrayList = parameterArrayList;
   }

   public long getForignId() {
      return forignId;
   }

   public void setForignId(long forignId) {
      this.forignId = forignId;
   }
}
