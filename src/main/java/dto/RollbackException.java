package dto;

public class RollbackException extends RuntimeException {
    public RollbackException(){
      super();
    }
    public RollbackException(String name){
        super(name);
    }
}
