package service;

import Entrance.Main;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import dao.JsonMapper;
import dto.FIleNotFoundException;
import dto.JsonData;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import net.sf.json.JsonConfig;

import javax.swing.filechooser.FileSystemView;
import java.io.File;
import java.io.FileOutputStream;
import java.util.List;
import java.util.Map;

public class ShowDateService {
    private JsonMapper jsonMapper = (JsonMapper) Main.context.getBean("jsonMapper");

    public List<JsonData> listJsonData(Map<String,Object> map){
        return jsonMapper.listJsonData(map);
    }

    public void createJson(JsonData jsonData)throws Exception{
        JsonData result = jsonMapper.returnJsonDataByFileName(jsonData);
        if(result==null){
            throw new FIleNotFoundException();
        }
        JsonConfig jsonConfig = new JsonConfig();
        String[] excludes = new String[]{"id","forignId"};
        jsonConfig.setExcludes(excludes);
        JSONObject jsonObject = JSONObject.fromObject(result,jsonConfig);
        JSONObject resultJsonObject = changeJsonObject(jsonObject);
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        FileSystemView fsv = FileSystemView.getFileSystemView();
        File com=fsv.getHomeDirectory();
        String path = com.getPath()+File.separator+"JsonData";
        File file = new File(path);
        if(!file.exists()){
            file.mkdir();
        }
        File file1 = new File(path+File.separator+resultJsonObject.get("JsonFileName"));
        if(file1.exists()){
            try {
                file.createNewFile();
            }catch (Exception e){
                //e.printStackTrace();
            }
        }
        FileOutputStream fileOutputStream = new FileOutputStream(file1);
        fileOutputStream.write(gson.toJson(resultJsonObject).getBytes());
        fileOutputStream.close();
    }

    public JSONObject changeJsonObject(JSONObject jsonObject){
        JSONObject jsonObject1 = new JSONObject();
        JSONObject cpJsonObejct = new JSONObject();
        JSONObject temp = (JSONObject)jsonObject.get("controlParameter");
        cpJsonObejct.put("ParmShowName",temp.get("parmShowName"));
        cpJsonObejct.put("ParmVal",temp.get("parmVal"));
        cpJsonObejct.put("ParmUD",temp.get("parmUD"));
        jsonObject1.put("ControlParameters",cpJsonObejct);
        JSONArray jsonArray =(JSONArray)jsonObject.get("processArrayList");
        JSONArray opJsonArray = new JSONArray();
        for(int i=0;i<jsonArray.size();i++){
            JSONObject jsonObject2 = jsonArray.getJSONObject(i);
            JSONObject opTemp = new JSONObject();
            opTemp.put("FKShowName",jsonObject2.get("FKShowName"));
            opTemp.put("WLShowName",jsonObject2.get("WLShowName"));
            opTemp.put("SetShowVal",jsonObject2.get("setShowVal"));
            opTemp.put("FKControlParameters",jsonObject2.get("parameterArrayList"));
            opTemp.put("BeginTime",jsonObject2.get("beginTime"));
            opTemp.put("EndTime",jsonObject2.get("endTime"));
            opTemp.put("EndState",jsonObject2.get("endState"));
            opJsonArray.add(opTemp);
        }
        jsonObject1.put("OperationProcessList",opJsonArray);
        jsonObject1.put("JsonFileName",jsonObject.get("jsonFileName"));
        jsonObject1.put("ProductName",jsonObject.get("productName"));
        jsonObject1.put("ProductDate",jsonObject.get("productDate"));
        jsonObject1.put("HarmonizationEquipment",jsonObject.get("harmonizationEquipment"));
        jsonObject1.put("ProductionQuantity",jsonObject.get("productionQuantity"));
        jsonObject1.put("ProcessNumber",jsonObject.get("processNumber"));
        jsonObject1.put("ProductionTemperature",jsonObject.get("productionTemperature"));
        jsonObject1.put("SamplingConclusion",jsonObject.get("samplingConclusion"));
        jsonObject1.put("PackingForm",jsonObject.get("packingForm"));
        jsonObject1.put("QuantityFilling",jsonObject.get("quantityFilling"));
        jsonObject1.put("FinalWeight",jsonObject.get("finalWeight"));
        jsonObject1.put("FillingLineNumber",jsonObject.get("fillingLineNumber"));
        jsonObject1.put("MainExercise",jsonObject.get("mainExercise"));
        jsonObject1.put("ToReview",jsonObject.get("toReview"));
        jsonObject1.put("ReportTime",jsonObject.get("reportTime"));
        return  jsonObject1;
    }
}
