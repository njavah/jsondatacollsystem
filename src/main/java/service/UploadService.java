package service;

import Entrance.Main;
import dao.JsonMapper;
import dao.ParameterMapper;
import dao.ProcessMapper;
import dto.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.TransactionCallback;
import org.springframework.transaction.support.TransactionCallbackWithoutResult;
import org.springframework.transaction.support.TransactionTemplate;
import panel.UploadPanel;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Observable;
import java.util.Observer;


public class UploadService extends Observable{
    private TransactionTemplate transactionTemplate =(TransactionTemplate) Main.context.getBean("transactionTemplate");
    private JsonMapper jsonMapper = (JsonMapper) Main.context.getBean("jsonMapper");
    private ParameterMapper parameterMapper = (ParameterMapper) Main.context.getBean("parameterMapper");
    private ProcessMapper processMapper = (ProcessMapper) Main.context.getBean("processMapper");
    private UploadPanel uploadPanel = null;
    private Logger logger = LoggerFactory.getLogger(UploadService.class);
    public UploadService(){

    }
    public UploadService(UploadPanel uploadPanel){
        this.uploadPanel = uploadPanel;
    }
    @Transactional(isolation= Isolation.DEFAULT,propagation=Propagation.REQUIRED,rollbackFor= Exception.class)
    public void insertData(final JsonData jsonData){
        transactionTemplate.execute(new TransactionCallbackWithoutResult() {
            @Override
            protected void doInTransactionWithoutResult(TransactionStatus status) {
                logger.info("insertData:data=>{}",jsonData.toString());
                if(jsonData!=null){
                    try {
                        AnalysisData.status = AnalysisStatus.STORE;
                        uploadPanel.update();
                        jsonMapper.insertJsonData(jsonData);
                        List<ControlParameter> parameterArrayList = new ArrayList<ControlParameter>();
                        parameterArrayList.add(jsonData.getControlParameter());
                        parameterMapper.insertParameter(parameterArrayList);
                        processMapper.insertProcess(jsonData.getProcessArrayList());
                        for(int i=0;i<jsonData.getProcessArrayList().size();i++){
                            ArrayList<ControlParameter> arrayList = jsonData.getProcessArrayList().get(i).getParameterArrayList();
                            for(int j=0;j<arrayList.size();j++){
                                arrayList.get(j).setForignId(jsonData.getProcessArrayList().get(i).getId());
                            }
                            parameterMapper.insertParameter(arrayList);
                        }
                        AnalysisData.status = AnalysisStatus.STORESUCCESS;
                        uploadPanel.update();
                    }catch (DuplicateKeyException duplicateKeyException){
                        logger.error("insertData:data=>{},Exception=>{},msg=>{}",jsonData.toString(),duplicateKeyException.getMessage(),duplicateKeyException);
                        JOptionPane.showMessageDialog(null, "该文件已存在", "【出错啦】", JOptionPane.ERROR_MESSAGE);
                    }catch (Exception e){
                        //记录日志
                        logger.error("insertData:data=>{},Exception=>{},msg=>{}",jsonData.toString(),e.getMessage(),e);
                    }finally {
                        AnalysisData.status = AnalysisStatus.STOREFAIL;
                        uploadPanel.update();

                    }
                }
            }
        });


    }

}
